<?php

/* 
this class is useful to convert HEX, RGB and HSL color codes, one to anothers
author: usman didi khamdani
author's email: usmankhamdani@gmail.com
author's phone: +6287883919293
last updated: Sept 18, 2010
*/

class colorConverter {

	function Hexa($dec) { // convert dec to hex

		if($dec<0 || $dec>255) {
			return false;
		} else {

			$p = floor($dec/16);
			$s = $dec%16;

			return dechex($p).dechex($s);

		}

	}

	function Deci($hex) { // convert hex to dec

		$HEXpar = preg_split('//','0123456789abcdef',-1,PREG_SPLIT_NO_EMPTY);
		
		if(strlen($hex)==1) {

			if(in_array($hex,$HEXpar)) {

				$dec = hexdec($hex);

				return ($dec * 16) + $dec;

			} else {
				return false;
			}

		} elseif(strlen($hex)==2) {

			$hex1 = substr($hex,0,1);
			$hex2 = substr($hex,1,1);

			if(in_array($hex1,$HEXpar) && in_array($hex2,$HEXpar)) {

				$dec1 = hexdec($hex1);
				$dec2 = hexdec($hex2);

				return ($dec1 * 16) + $dec2;

			} else {
				return false;
			}

		} else {
			return false;

		}

	}

	function isRGB($R,$G,$B) { // check validity of RGB color code

		if($this->Hexa($R)==false || $this->Hexa($G)==false || $this->Hexa($B)==false) {

			$this->RGBError = 1;

			if($this->Hexa($R)==false) {
				$Re = 'Minimal value of Red color is 0 and maximal value is 255<br />';
			} else {
				$Re = '';
			}

			if($this->Hexa($G)==false) {
				$Ge = 'Minimal value of Green color is 0 and maximal value is 255<br />';
			} else {
				$Ge = '';
			}

			if($this->Hexa($B)==false) {
				$Be = 'Minimal value of Blue color is 0 and maximal value is 255<br />';
			} else {
				$Be = '';
			}

			$this->RGBErrorMessage = '<div><b>Error RGB Color Value!</b><br />'.$Re.$Ge.$Be.'</div>';

		} else {
			$this->RGBError = 0;
		}

	}

	function isHEX($HEX) { // check validity of HEX color code

		$HEX = strtolower(str_replace('#','',$HEX));
		$HEX = preg_split('//',$HEX,-1,PREG_SPLIT_NO_EMPTY);
		$HEXpar = preg_split('//','0123456789abcdef',-1,PREG_SPLIT_NO_EMPTY);
		$HEXlen = count($HEX);

		/*
		// for css rule only
		if($HEXlen==3 || $HEXlen==6) {

			if($HEXlen==3) {

				if(in_array($HEX[0],$HEXpar) && in_array($HEX[1],$HEXpar) && in_array($HEX[2],$HEXpar)) {
					$this->HEXError = 0;
				} else {

					$this->HEXError = 1;
					$this->HEXErrorMessage = '<div><b>Error HEX Color Value!</b><br />Minimal value of each HEX color is 0 and maximal value is f</div>';

				}

			} else {

				if(in_array($HEX[0],$HEXpar) && in_array($HEX[1],$HEXpar) && in_array($HEX[2],$HEXpar) && in_array($HEX[3],$HEXpar) && in_array($HEX[4],$HEXpar) && in_array($HEX[5],$HEXpar)) {
					$this->HEXError = 0;
				} else {

					$this->HEXError = 1;
					$this->HEXErrorMessage = '<div><b>Error HEX Color Value!</b><br />Minimal value of each HEX color is 0 and maximal value is f</div>';

				}

			}
		///////////////////////////////////
		*/
		// for all rule, include css
		if($HEXlen==6) {

			if(in_array($HEX[0],$HEXpar) && in_array($HEX[1],$HEXpar) && in_array($HEX[2],$HEXpar) && in_array($HEX[3],$HEXpar) && in_array($HEX[4],$HEXpar) && in_array($HEX[5],$HEXpar)) {
				$this->HEXError = 0;
			} else {

				$this->HEXError = 1;
				$this->HEXErrorMessage = '<div><b>Error HEX Color Value!</b><br />Minimal value of each HEX color is 0 and maximal value is f</div>';

			}
		///////////////////////////////////
		} else {
			$this->HEXError = 1;
			$this->HEXErrorMessage = '<div><b>Error HEX Color Value!</b><br />Lenght of Hex color must be 3 or 6 characters</div>';
		}

	}

	function isHSL($H,$S,$L) { // check validity of HSL color code

		if(($H<0 || $S<0 || $L<0) || ($H>360 || $S>100 || $L>100)) {
			$this->HSLError = 1;

			if($H<0 || $H>360) {
				$He = 'Minimal value of Hue color is 0 and maximal value is 360<br />';
			} else {
				$He = '';
			}
			if($S<0 || $S>100) {
				$Se = 'Minimal value of Saturation color is 0 and maximal value is 100<br />';
			} else {
				$Se = '';
			}
			if($L<0 || $L>100) {
				$Le = 'Minimal value of Lightness color is 0 and maximal value is 100';
			} else {
				$Le = '';
			}

			$this->HSLErrorMessage = '<div><b>Error HSL Color Value!</b><br />'.$He.$Se.$Le.'</div>';

		} else {
			$this->HSLError = 0;
		}

	}

	function RGB2HEX($R,$G,$B) { // convert RGB to HEX color

		$this->isRGB($R,$G,$B);

		if($this->RGBError==0) {

			$Rh = $this->Hexa($R);
			$Gh = $this->Hexa($G);
			$Bh = $this->Hexa($B);

			$Rh1 = substr($Rh,0,1);
			$Rh2 = substr($Rh,1,1);

			$Gh1 = substr($Gh,0,1);
			$Gh2 = substr($Gh,1,1);

			$Bh1 = substr($Bh,0,1);
			$Bh2 = substr($Bh,1,1);
			
			/*
			// for css rule only
			if(($Rh1==$Rh2) && ($Gh1==$Gh2) && ($Bh1==$Bh2)) {
				return '#'.$Rh1.$Gh1.$Bh1;
			} else {
				return '#'.$Rh.$Gh.$Bh;
			}
			*/
			return '#'.$Rh.$Gh.$Bh; // for all rule, include css

		}
	}

	function HEX2RGB($HEX) { // convert HEX to RGB color

		$this->isHEX($HEX);

		if($this->HEXError==0) {

			$HEX = str_replace('#','',$HEX);

			if(strlen($HEX)==3) {

				$R = $this->Deci(substr($HEX,0,1));
				$G = $this->Deci(substr($HEX,1,1));
				$B = $this->Deci(substr($HEX,2,1));

			} else {

				$R = $this->Deci(substr($HEX,0,2));
				$G = $this->Deci(substr($HEX,2,2));
				$B = $this->Deci(substr($HEX,4,2));

			}

			return array($R,$G,$B);

		}

	}

	function RGB2HSL($R,$G,$B) { // convert RGB to HSL color; adapted from www.easyrgb.com

		$this->isRGB($R,$G,$B);

		if($this->RGBError==0) {

			$R = $R/255; 
			$G = $G/255;
			$B = $B/255;

			$RGB = array($R,$G,$B);
			sort($RGB);

			$min = $RGB[0];  
			$max = $RGB[2]; 
			$delta = $max-$min;   

			$L = ($max+$min)/2;

			if($delta==0)  {              
				$H = 0;                             
				$S = 0;
			} else {                                
				if($L<0.5) {
					$S = $delta/($max+$min);
				} else {
					$S = $delta/(2-$max-$min);
				}

				$Rn = ((($max-$R)/6)+($delta/2))/$delta;
				$Gn = ((($max-$G)/6)+($delta/2))/$delta;
				$Bn = ((($max-$B)/6)+($delta/2))/$delta;

				if($R==$max) {
					$H = $Bn-$Gn;
				}
				if($G==$max) {
					$H = (1/3)+$Rn-$Bn;
				}
				if($B==$max) {
					$H = (2/3)+$Gn-$Rn;
				}

				if($H<0) {
					$H = $H+1;
				}
				if($H>1) {
					$H = $H-1;
				}
			}

			$H = $H*360;
			$S = $S*100;
			$L = $L*100;

			return array($H,$S,$L);

		}

	}	

	function HSL2RGB($H,$S,$L) { // convert HSL to RGB color; adapted from www.easyrgb.com

		$this->isHSL($H,$S,$L);

		if($this->HSLError==0) {

			$H = $H/360; 
			$S = $S/100;
			$L = $L/100;

			if($S==0) {

				$R = $L*255;
				$G = $L*255;
				$B = $L*255;

			} else {

				if($L<0.5) {
					$temp2 = $L*(1+$S);
				} else {
					$temp2 = ($L+$S)-($S*$L);
				}

				$temp1 = (2*$L)-$temp2;

				$Rtemp3 = $H+(1/3);
				$Gtemp3 = $H;
				$Btemp3 = $H-(1/3);

				$R = 255*$this->Hue2RGB($temp1,$temp2,$Rtemp3);
				$G = 255*$this->Hue2RGB($temp1,$temp2,$Gtemp3);
				$B = 255*$this->Hue2RGB($temp1,$temp2,$Btemp3);
			}

			return array($R,$G,$B);

		}

	}

	function Hue2RGB($temp1,$temp2,$temp3) { // part of HSL2RGB function

		if($temp3<0) {
			$temp3 = $temp3+1;
		}
		if($temp3>1) {
			$temp3 = $temp3-1;
		}

		if((6*$temp3)<1) {
			return $temp1+($temp2-$temp1)*6*$temp3;
		} elseif((2*$temp3)<1) {
			return $temp2;
		} elseif((3*$temp3)<2) {
			return $temp1+($temp2-$temp1)*((2/3)-$temp3)*6;
		} else {
			return $temp1;
		}

	}

	function HEX2HSL($HEX) { // convert HEX to HSL color

		$this->isHEX($HEX);

		if($this->HEXError==0) {

			$temp = $this->HEX2RGB($HEX);

			return $this->RGB2HSL($temp[0],$temp[1],$temp[2]);

		}

	}

	function HSL2HEX($H,$S,$L) { // convert HSL to HEX color

		$this->isHSL($H,$S,$L);

		if($this->HSLError==0) {

			$temp = $this->HSL2RGB($H,$S,$L);

			return $this->RGB2HEX($temp[0],$temp[1],$temp[2]);

		}

	}

}

?>
<?php

// last updated: Sept 18, 2010

include('color_converter.class.php');

class colorHarmony extends colorConverter {

	function Monochromatic($HEX) { // Monochromatic
		
		$color1 = strtolower($HEX); // base color

		$RGB = $this->HEX2RGB($color1);
		$R = $RGB[0];
		$G = $RGB[1];
		$B = $RGB[2];

		$R2 = $this->MonoC($R,2);
		$G2 = $this->MonoC($G,2);
		$B2 = $this->MonoC($B,2);

		$color2 = $this->RGB2HEX($R2,$G2,$B2);

		$R3 = $this->MonoC($R,3);
		$G3 = $this->MonoC($G,3);
		$B3 = $this->MonoC($B,3);

		$color3 = $this->RGB2HEX($R3,$G3,$B3);

		$R4 = $this->MonoC($R,4);
		$G4 = $this->MonoC($G,4);
		$B4 = $this->MonoC($B,4);

		$color4 = $this->RGB2HEX($R4,$G4,$B4);

		$color5 = $this->SecondC($color1);
		$color6 = $this->SecondC($color2);
		$color7 = $this->SecondC($color3);
		$color8 = $this->SecondC($color4);
		$color9 = $this->SecondC($color5);

		return array($color1,$color2,$color3,$color4,$color5,$color6,$color7,$color8,$color9);

	}

	function Analogous($HEX) { // Analogous
		
		$color1 = strtolower($HEX); // base color

		$temp = $this->HEX2HSL($color1);

		$H = $temp[0];
		$S = $temp[1];
		$L = $temp[2];

		$H1 = $this->FixHue($H+30);
		$H2 = $this->FixHue($H-30);

		$color2 = $this->HSL2HEX($H1,$S,$L);
		/*
		$color3 = '#fff'; // for css rule only
		*/
		$color3 = '#ffffff'; // for all rule, include css
		$color4 = $this->HSL2HEX($H2,$S,$L);

		$color5 = $this->SecondC($color1);
		$color6 = $this->SecondC($color2);
		$color7 = $this->SecondC($color3);
		$color8 = $this->SecondC($color4);
		$color9 = $this->SecondC($color5);

		return array($color1,$color2,$color3,$color4,$color5,$color6,$color7,$color8,$color9);

	}

	function Complementary($HEX) { // Complementary
		
		$color1 = strtolower($HEX); // base color

		$RGB = $this->HEX2RGB($color1);
		$R = $RGB[0];
		$G = $RGB[1];
		$B = $RGB[2];

		$temp = $this->HEX2HSL($HEX);

		$H = $temp[0];
		$H = $this->FixHue($H+180);
		$S = $temp[1];
		$L = $temp[2];

		$color2 = $this->HSL2HEX($H,$S,$L);

		$RGB2 = $this->HEX2RGB($color2);
		$R2 = $RGB2[0];
		$G2 = $RGB2[1];
		$B2 = $RGB2[2];

		$R = $this->MonoC($R,2);
		$G = $this->MonoC($G,2);
		$B = $this->MonoC($B,2);

		$color3 = $this->RGB2HEX($R,$G,$B);

		$R2 = $this->MonoC($R2,2);
		$G2 = $this->MonoC($G2,2);
		$B2 = $this->MonoC($B2,2);

		$color4 = $this->RGB2HEX($R2,$G2,$B2);

		$color5 = $this->SecondC($color1);
		$color6 = $this->SecondC($color2);
		$color7 = $this->SecondC($color3);
		$color8 = $this->SecondC($color4);
		$color9 = $this->SecondC($color5);

		return array($color1,$color2,$color3,$color4,$color5,$color6,$color7,$color8,$color9);

	}

	function Triads($HEX) { // Triads
		
		$color1 = strtolower($HEX); // base color

		$temp = $this->HEX2HSL($color1);

		$H = $temp[0];
		$S = $temp[1];
		$L = $temp[2];

		$H1 = $this->FixHue($H+120);
		$H2 = $this->FixHue($H-120);

		$color2 = $this->HSL2HEX($H1,$S,$L);
		/*
		$color3 = '#fff'; // for css rule only
		*/
		$color3 = '#ffffff'; // for all rule, include css
		$color4 = $this->HSL2HEX($H2,$S,$L);

		$color5 = $this->SecondC($color1);
		$color6 = $this->SecondC($color2);
		$color7 = $this->SecondC($color3);
		$color8 = $this->SecondC($color4);
		$color9 = $this->SecondC($color5);

		return array($color1,$color2,$color3,$color4,$color5,$color6,$color7,$color8,$color9);

	}

	function FixHue($Hue) {

		if($Hue<0) {
			return $Hue+360;
		} elseif($Hue>360) {
			return $Hue-360;
		} else {
			return $Hue;
		}

	}

	function SecondC($c) {

		$RGB = $this->HEX2RGB($c);
		$R = $RGB[0];
		$G = $RGB[1];
		$B = $RGB[2];

		$par = 0.75;

		$R2 = floor($par*$R);
		$G2 = floor($par*$G);
		$B2 = floor($par*$B);

		return $this->RGB2HEX($R2,$G2,$B2);

	}

	function MonoC($c,$n) {

		$par1 = 128;
		$par2 = 192;
		$par3 = 64;
		$par4 = 223;

		$diffC = $c-$par1; // $c = color
		$diff = abs($diffC);

		if($n==2) {
			if($diffC>=1) {
				return $par2+floor(0.5*$diff);
			} elseif($diffC<0) {
				return $par2-floor(0.5*$diff);
			} else {
				return $par2;
			}
		} 
		
		if($n==3) {
			if($diffC>=1) {
				return $par3+floor(0.5*$diff);
			} elseif($diffC<0) {
				return $par3-floor(0.5*$diff);
			} else {
				return $par3;
			}
		} 
		
		if($n==4) {
			if($diffC>=1) {
				return $par4+floor(0.25*$diff);
			} elseif($diffC<0) {
				return $par4-floor(0.25*$diff);
			} else {
				return $par4;
			}
		} 

	}

}




//////



// Définition couleur N1

	$c = new colorHarmony;  
	$hex = $background;	
	$c->isHEX($hex);

	//if($c->HEXError==0) {

			// Monochromatic
			$RH1= $c->Monochromatic($hex);			

			for($i=0;$i<9;$i++) {
				$RGB1[$i] = $c->HEX2RGB($RH1[$i]);
			
			}

			// Analogous
			$RH2= $c->Analogous($hex);			

			for($i=0;$i<9;$i++) {
				$RGB2[$i] = $c->HEX2RGB($RH2[$i]);
			}
			// Complementary
			$RH3= $c->Complementary($hex);			

			for($i=0;$i<9;$i++) {
				$RGB3[$i] = $c->HEX2RGB($RH3[$i]);
			}
			// Triads
			$RH4= $c->Triads($hex);			

			for($i=0;$i<9;$i++) {
				$RGB4[$i] = $c->HEX2RGB($RH4[$i]);
			}


	// Choix couleur N1


			$color_1 = $RH2[1];
			$color_2 = $RH2[6];
			$color_3 = $RH2[3];
			$color_4 = $RH2[4];
			$color_5 = $RH2[5];
			$color_6 = $RH2[7];
			$color_7 = $RH2[8];




	// Définition couleur N2

			$c_1 = new colorHarmony;  
			$hex = $color_1;	
			$c_1->isHEX($hex);

			// Triads
			$RH4_1= $c_1->Analogous($hex);			

			for($i=0;$i<8;$i++) {
				$RGB4_1[$i] = $c_1->HEX2RGB($RH4_1[$i]);
			}

	// Choix couleur N2
				
			$color_1_1 = $RH4_1[1];
			$color_1_2 = $RH4_1[2];
			$color_1_3 = $RH4_1[3];
			$color_1_4 = $RH4_1[4];

?>




<style type='text/css'>


.atrium-welcome-links a:hover,
.dropdown-blocks .block-toggle li a:hover,
.dropdown-blocks .block-toggle li a:focus,
.dropdown-blocks .block-toggle ul.links li a:hover,
 .dropdown-blocks .block-toggle ul.links li a:focus,
 .dropdown-blocks .block-toggle .item-list li a:hover,
 .dropdown-blocks .block-toggle .item-list li a:focus,
 .pager li.pager-current,
.more-link a:hover,
 #global,
  #navigation { background-color:<?php print $background ?>; }

  #header .block-widget .block-content,
  #header .block .block-title { background-color:<?php print designkit_colorshift($background, '#000000', .15) ?>; }
  .page-region .block .block-title {
  background-color:<?php print designkit_colorshift($background, '#eeeeee', .1) ?>;
  border-color:<?php print designkit_colorshift($background, '#dddddd', .1) ?>;
  border-bottom-color:<?php print designkit_colorshift($background, '#cccccc',.1) ?>;
  }
h2.node-title a, 
.page-blog .view-blog-listing h2.node-title a 
{ color:<?php print designkit_colorshift($background, '#666666', 1.0) ?>; 
}
.node-submitted a.username 
{ color:<?php print designkit_colorshift($background, '#ffffff', .25) ?>; 
}
div.node .field.terms li, 
div.node .field.field-type-nodereference a 
{ background-color:<?php print designkit_colorshift($background, '#ffffff', .9) ?>; 
}
div.node-content.clear-block.prose div.field.terms.clear-block span.field-label-inline-first 
{ background-color:<?php print designkit_colorshift($background, '#eeeeee', .1) ?>; 
}
div.node-content div.field-items div.field-item.odd div.field-label-inline-first 
{ background-color:<?php print designkit_colorshift($background, '#eeeeee', .1) ?>; }

span.spaces-feature { background-color:<?php print designkit_colorshift($background, '#eeeeee', .15) ?>; }



body.page-dashboard .atrium-welcome-links a:hover,
body.page-dashboard .dropdown-blocks .block-toggle li a:hover,
body.page-dashboard .dropdown-blocks .block-toggle li a:focus,
body.page-dashboard .dropdown-blocks .block-toggle ul.links li a:hover,
body.page-dashboard .dropdown-blocks .block-toggle ul.links li a:focus,
body.page-dashboard .dropdown-blocks .block-toggle .item-list li a:hover,
body.page-dashboard .dropdown-blocks .block-toggle .item-list li a:focus,
body.page-dashboard .pager li.pager-current,
body.page-dashboard .more-link a:hover,
body.page-dashboard #global,
body.page-dashboard  #navigation { background-color:<?php print $background ?>; }

body.page-dashboard  #header .block-widget .block-content,
body.page-dashboard  #header .block .block-title { background-color:<?php print designkit_colorshift($background, '#000000', .15) ?>; }
body.page-dashboard  .page-region .block .block-title {
  background-color:<?php print designkit_colorshift($background, '#eeeeee', .1) ?>;
  border-color:<?php print designkit_colorshift($background, '#dddddd', .1) ?>;
  border-bottom-color:<?php print designkit_colorshift($background, '#cccccc',.1) ?>;
  }
body.page-dashboard  h2.node-title a, 
body.page-dashboard .page-blog .view-blog-listing h2.node-title a 
{ color:<?php print designkit_colorshift($background, '#666666', 1.0) ?>; 
}
body.page-dashboard .node-submitted a.username 
{ color:<?php print designkit_colorshift($background, '#ffffff', .25) ?>; 
}
body.page-dashboard div.node .field.terms li, 
body.page-dashboard div.node .field.field-type-nodereference a 
{ background-color:<?php print designkit_colorshift($background, '#ffffff', .9) ?>; 
}
body.page-dashboard div.node-content.clear-block.prose div.field.terms.clear-block span.field-label-inline-first 
{ background-color:<?php print designkit_colorshift($background, '#eeeeee', .1) ?>; 
}
body.page-dashboard div.node-content div.field-items div.field-item.odd div.field-label-inline-first 
{ background-color:<?php print designkit_colorshift($background, '#eeeeee', .1) ?>; }

body.page-dashboard  span.spaces-feature { background-color:<?php print designkit_colorshift($background, '#eeeeee', .15) ?>; }

body.page-dashboard   div.litecal .litecal-title { color:<?php print designkit_colorshift($background, '#eeeeee', .1) ?>; 
    color:;
}

body.page-dashboard #block-views-blog_comments-block_1 div.views-field.related-title a:before { background-color:<?php print designkit_colorshift($color_1, '#eeeeee', .1) ?>; }
table.cases tr.mine td.username { background-color:<?php print designkit_colorshift($color_1, '#eeeeee', .5) ?>; }



body.node-type-blog .atrium-welcome-links a:hover,
body.node-type-blog .dropdown-blocks .block-toggle li a:hover,
body.node-type-blog .dropdown-blocks .block-toggle li a:focus,
body.node-type-blog .dropdown-blocks .block-toggle ul.links li a:hover,
body.node-type-blog .dropdown-blocks .block-toggle ul.links li a:focus,
body.node-type-blog .dropdown-blocks .block-toggle .item-list li a:hover,
body.node-type-blog .dropdown-blocks .block-toggle .item-list li a:focus,
body.node-type-blog .pager li.pager-current,
body.node-type-blog .more-link a:hover,
body.node-type-blog #global,
body.node-type-blog #navigation { background-color:<?php print $color_1 ?>; }
body.node-type-blog #header .block-widget .block-content,
body.node-type-blog #header .block .block-title { background-color:<?php print designkit_colorshift($color_1, '#000000', .15) ?>; }
body.node-type-blog .page-region .block .block-title {
  background-color:<?php print designkit_colorshift($color_1, '#eeeeee', .1) ?>;
  border-color:<?php print designkit_colorshift($color_1, '#dddddd', .1) ?>;
  border-bottom-color:<?php print designkit_colorshift($color_1, '#cccccc',.1) ?>;
  }
body.node-type-blog h2.node-title a, 
body.node-type-blog .page-blog .view-blog-listing h2.node-title a 
{ color:<?php print designkit_colorshift($color_1, '#666666', 1.0) ?>; 
}
body.node-type-blog .node-submitted a.username 
{ color:<?php print designkit_colorshift($color_1, '#ffffff', .25) ?>; 
}
body.node-type-blog div.node .field.terms li, 
body.node-type-blog div.node .field.field-type-nodereference a 
{ background-color:<?php print designkit_colorshift($color_1, '#ffffff', .9) ?>; 
}
body.node-type-blog div.node-content.clear-block.prose div.field.terms.clear-block span.field-label-inline-first 
{ background-color:<?php print designkit_colorshift($color_1, '#eeeeee', .1) ?>; 
}
body.node-type-blog div.node-content div.field-items div.field-item.odd div.field-label-inline-first 
{ background-color:<?php print designkit_colorshift($color_1, '#eeeeee', .1) ?>; }
body.node-type-blog h2.comment-title a { background-color:<?php print designkit_colorshift($color_1, '#eeeeee', .1) ?>; 
}


body.page-blog .atrium-welcome-links a:hover,
body.page-blog .dropdown-blocks .block-toggle li a:hover,
body.page-blog .dropdown-blocks .block-toggle li a:focus,
body.page-blog .dropdown-blocks .block-toggle ul.links li a:hover,
body.page-blog .dropdown-blocks .block-toggle ul.links li a:focus,
body.page-blog .dropdown-blocks .block-toggle .item-list li a:hover,
body.page-blog .dropdown-blocks .block-toggle .item-list li a:focus,
body.page-blog .pager li.pager-current,
body.page-blog .more-link a:hover,
body.page-blog #global,
body.page-blog #navigation { background-color:<?php print $color_1 ?>; }
body.page-blog #header .block-widget .block-content,
body.page-blog #header .block .block-title { background-color:<?php print designkit_colorshift($color_1, '#000000', .15) ?>; }
body.page-blog .page-region .block .block-title {
  background-color:<?php print designkit_colorshift($color_1, '#eeeeee', .1) ?>;
  border-color:<?php print designkit_colorshift($color_1, '#dddddd', .1) ?>;
  border-bottom-color:<?php print designkit_colorshift($color_1, '#cccccc',.1) ?>;
  }
body.page-blog h2.node-title a, 
body.page-blog .page-blog .view-blog-listing h2.node-title a 
{ color:<?php print designkit_colorshift($color_1, '#666666', 1.0) ?>; 
}
body.page-blog .node-submitted a.username 
{ color:<?php print designkit_colorshift($color_1, '#ffffff', .25) ?>; 
}
body.page-blog div.node .field.terms li, 
body.page-blog div.node .field.field-type-nodereference a 
{ background-color:<?php print designkit_colorshift($color_1, '#ffffff', .9) ?>; 
}
body.page-blog div.node-content.clear-block.prose div.field.terms.clear-block span.field-label-inline-first 
{ background-color:<?php print designkit_colorshift($color_1, '#eeeeee', .1) ?>; 
}
body.page-blog div.node-content div.field-items div.field-item.odd div.field-label-inline-first 
{ background-color:<?php print designkit_colorshift($color_1, '#eeeeee', .1) ?>; }



body.page-notebook .atrium-welcome-links a:hover,
body.page-notebook .dropdown-blocks .block-toggle li a:hover,
body.page-notebook .dropdown-blocks .block-toggle li a:focus,
body.page-notebook .dropdown-blocks .block-toggle ul.links li a:hover,
body.page-notebook .dropdown-blocks .block-toggle ul.links li a:focus,
body.page-notebook .dropdown-blocks .block-toggle .item-list li a:hover,
body.page-notebook .dropdown-blocks .block-toggle .item-list li a:focus,
body.page-notebook .pager li.pager-current,
body.page-notebook .more-link a:hover,
body.page-notebook #global,
body.page-notebook  #navigation { background-color:<?php print $color_2 ?>; }
body.page-notebook  .atrium-welcome-links a:hover,
body.page-notebook  .dropdown-blocks .block-toggle li a:hover,
body.page-notebook  .dropdown-blocks .block-toggle li a:focus,
body.page-notebook  .dropdown-blocks .block-toggle ul.links li a:hover,
body.page-notebook  .dropdown-blocks .block-toggle ul.links li a:focus,
body.page-notebook  .dropdown-blocks .block-toggle .item-list li a:hover,
body.page-notebook  .dropdown-blocks .block-toggle .item-list li a:focus,
body.page-notebook  .pager li.pager-current,
body.page-notebook  .more-link a:hover,
body.page-notebook  #global,
body.page-notebook  #navigation { background-color:<?php print $color_2 ?>; }
body.page-notebook  #header .block-widget .block-content,
body.page-notebook  #header .block .block-title { background-color:<?php print designkit_colorshift($color_2, '#000000', .15) ?>; }
body.page-notebook  .page-region .block .block-title {
  background-color:<?php print designkit_colorshift($color_2, '#eeeeee', .1) ?>;
  border-color:<?php print designkit_colorshift($color_2, '#dddddd', .1) ?>;
  border-bottom-color:<?php print designkit_colorshift($color_2, '#cccccc',.1) ?>;
  }
body.page-notebook  h2.node-title a, 
body.page-notebook  .page-blog .view-blog-listing h2.node-title a 
{ color:<?php print designkit_colorshift($color_2, '#666666', 1.0) ?>; 
}
body.page-notebook  .node-submitted a.username 
{ color:<?php print designkit_colorshift($color_2, '#ffffff', .25) ?>; 
}
body.page-notebook  div.node .field.terms li, 
body.page-notebook  div.node .field.field-type-nodereference a 
{ background-color:<?php print designkit_colorshift($color_2, '#ffffff', .9) ?>; 
}
body.page-notebook  div.node-content.clear-block.prose div.field.terms.clear-block span.field-label-inline-first 
{ background-color:<?php print designkit_colorshift($color_2, '#eeeeee', .1) ?>; 
}
body.page-notebook  div.node-content div.field-items div.field-item.odd div.field-label-inline-first 
{ background-color:<?php print designkit_colorshift($color_2, '#eeeeee', .1) ?>; }


body.page-notebook  span.spaces-feature { background-color:<?php print designkit_colorshift($color_2, '#eeeeee', .1) ?>; 
}


body.node-type-book .atrium-welcome-links a:hover,
body.node-type-book .dropdown-blocks .block-toggle li a:hover,
body.node-type-book .dropdown-blocks .block-toggle li a:focus,
body.node-type-book .dropdown-blocks .block-toggle ul.links li a:hover,
body.node-type-book .dropdown-blocks .block-toggle ul.links li a:focus,
body.node-type-book .dropdown-blocks .block-toggle .item-list li a:hover,
body.node-type-book .dropdown-blocks .block-toggle .item-list li a:focus,
body.node-type-book .pager li.pager-current,
body.node-type-book .more-link a:hover,
body.node-type-book #global,
body.node-type-book  #navigation { background-color:<?php print $color_2 ?>; }
body.node-type-book  .atrium-welcome-links a:hover,
body.node-type-book  .dropdown-blocks .block-toggle li a:hover,
body.node-type-book  .dropdown-blocks .block-toggle li a:focus,
body.node-type-book  .dropdown-blocks .block-toggle ul.links li a:hover,
body.node-type-book  .dropdown-blocks .block-toggle ul.links li a:focus,
body.node-type-book  .dropdown-blocks .block-toggle .item-list li a:hover,
body.node-type-book  .dropdown-blocks .block-toggle .item-list li a:focus,
body.node-type-book  .pager li.pager-current,
body.node-type-book  .more-link a:hover,
body.node-type-book  #global,
body.node-type-book  #navigation { background-color:<?php print $color_2 ?>; }
body.node-type-book  #header .block-widget .block-content,
body.node-type-book  #header .block .block-title { background-color:<?php print designkit_colorshift($color_2, '#000000', .15) ?>; }
body.node-type-book  .page-region .block .block-title {
  background-color:<?php print designkit_colorshift($color_2, '#eeeeee', .1) ?>;
  border-color:<?php print designkit_colorshift($color_2, '#dddddd', .1) ?>;
  border-bottom-color:<?php print designkit_colorshift($color_2, '#cccccc',.1) ?>;
  }
body.node-type-book  h2.node-title a, 
body.node-type-book  .page-blog .view-blog-listing h2.node-title a 
{ color:<?php print designkit_colorshift($color_2, '#666666', 1.0) ?>; 
}
body.node-type-book  .node-submitted a.username 
{ color:<?php print designkit_colorshift($color_2, '#ffffff', .25) ?>; 
}
body.node-type-book  div.node .field.terms li, 
body.node-type-book  div.node .field.field-type-nodereference a 
{ background-color:<?php print designkit_colorshift($color_2, '#ffffff', .9) ?>; 
}
body.node-type-book  div.node-content.clear-block.prose div.field.terms.clear-block span.field-label-inline-first 
{ background-color:<?php print designkit_colorshift($color_2, '#eeeeee', .1) ?>; 
}
body.node-type-book  div.node-content div.field-items div.field-item.odd div.field-label-inline-first 
{ background-color:<?php print designkit_colorshift($color_2, '#eeeeee', .1) ?>; }


body.node-type-book  span.spaces-feature { background-color:<?php print designkit_colorshift($color_2, '#eeeeee', .1) ?>; 
}


body.page-calendar .atrium-welcome-links a:hover,
body.page-calendar .dropdown-blocks .block-toggle li a:hover,
body.page-calendar .dropdown-blocks .block-toggle li a:focus,
body.page-calendar .dropdown-blocks .block-toggle ul.links li a:hover,
body.page-calendar .dropdown-blocks .block-toggle ul.links li a:focus,
body.page-calendar .dropdown-blocks .block-toggle .item-list li a:hover,
body.page-calendar .dropdown-blocks .block-toggle .item-list li a:focus,
body.page-calendar .pager li.pager-current,
body.page-calendar .more-link a:hover,
body.page-calendar #global,
body.page-calendar #navigation { background-color:<?php print $color_3 ?>; }
body.page-calendar  .atrium-welcome-links a:hover,
body.page-calendar  .dropdown-blocks .block-toggle li a:hover,
body.page-calendar  .dropdown-blocks .block-toggle li a:focus,
body.page-calendar  .dropdown-blocks .block-toggle ul.links li a:hover,
body.page-calendar  .dropdown-blocks .block-toggle ul.links li a:focus,
body.page-calendar  .dropdown-blocks .block-toggle .item-list li a:hover,
body.page-calendar  .dropdown-blocks .block-toggle .item-list li a:focus,
body.page-calendar  .pager li.pager-current,
body.page-calendar  .more-link a:hover,
body.page-calendar  #global,
body.page-calendar  #navigation { background-color:<?php print $color_3 ?>; }
body.page-calendar  #header .block-widget .block-content,
body.page-calendar  #header .block .block-title { background-color:<?php print designkit_colorshift($color_3, '#000000', .15) ?>; }
body.page-calendar  .page-region .block .block-title {
  background-color:<?php print designkit_colorshift($color_3, '#eeeeee', .1) ?>;
  border-color:<?php print designkit_colorshift($color_3, '#dddddd', .1) ?>;
  border-bottom-color:<?php print designkit_colorshift($color_3, '#cccccc',.1) ?>;
  }
body.page-calendar  h2.node-title a, 
body.page-calendar  .page-blog .view-blog-listing h2.node-title a 
{ color:<?php print designkit_colorshift($color_3, '#666666', 1.0) ?>; 
}
body.page-calendar div.litecal .litecal-title h3.litecal-title  
{ color:<?php print designkit_colorshift($color_3, '#666666', 0.5) ?>; 
}
body.page-calendar  .node-submitted a.username 
{ color:<?php print designkit_colorshift($color_3, '#ffffff', .25) ?>; 
}
body.page-calendar  div.node .field.terms li, 
body.page-calendar  div.node .field.field-type-nodereference a 
{ background-color:<?php print designkit_colorshift($color_3, '#ffffff', .9) ?>; 
}
body.page-calendar  div.node-content.clear-block.prose div.field.terms.clear-block span.field-label-inline-first 
{ background-color:<?php print designkit_colorshift($color_3, '#eeeeee', .1) ?>; 
}
body.page-calendar  div.node-content div.field-items div.field-item.odd div.field-label-inline-first 
{ background-color:<?php print designkit_colorshift($color_3, '#eeeeee', .1) ?>; }

body.page-calendar #right .page-region .block .block-title 
{ background-color:<?php print designkit_colorshift($color_3, '#eeeeee', .1) ?>; }



body.node-type-event.atrium-welcome-links a:hover,
body.node-type-event.dropdown-blocks .block-toggle li a:hover,
body.node-type-event.dropdown-blocks .block-toggle li a:focus,
body.node-type-event.dropdown-blocks .block-toggle ul.links li a:hover,
body.node-type-event.dropdown-blocks .block-toggle ul.links li a:focus,
body.node-type-event.dropdown-blocks .block-toggle .item-list li a:hover,
body.node-type-event.dropdown-blocks .block-toggle .item-list li a:focus,
body.node-type-event.pager li.pager-current,
body.node-type-event.more-link a:hover,
body.node-type-event#global,
body.node-type-event#navigation { background-color:<?php print $color_3 ?>; }
body.node-type-event .atrium-welcome-links a:hover,
body.node-type-event .dropdown-blocks .block-toggle li a:hover,
body.node-type-event .dropdown-blocks .block-toggle li a:focus,
body.node-type-event .dropdown-blocks .block-toggle ul.links li a:hover,
body.node-type-event .dropdown-blocks .block-toggle ul.links li a:focus,
body.node-type-event .dropdown-blocks .block-toggle .item-list li a:hover,
body.node-type-event .dropdown-blocks .block-toggle .item-list li a:focus,
body.node-type-event .pager li.pager-current,
body.node-type-event .more-link a:hover,
body.node-type-event #global,
body.node-type-event #navigation { background-color:<?php print $color_3 ?>; }
body.node-type-event #header .block-widget .block-content,
body.node-type-event #header .block .block-title { background-color:<?php print designkit_colorshift($color_3, '#000000', .15) ?>; }
body.node-type-event .page-region .block .block-title {
  background-color:<?php print designkit_colorshift($color_3, '#eeeeee', .1) ?>;
  border-color:<?php print designkit_colorshift($color_3, '#dddddd', .1) ?>;
  border-bottom-color:<?php print designkit_colorshift($color_3, '#cccccc',.1) ?>;
  }
body.node-type-event h2.node-title a, 
body.node-type-event .page-blog .view-blog-listing h2.node-title a 
{ color:<?php print designkit_colorshift($color_3, '#666666', 1.0) ?>; 
}
body.node-type-event .node-submitted a.username 
{ color:<?php print designkit_colorshift($color_3, '#ffffff', .25) ?>; 
}
body.node-type-event div.node .field.terms li, 
body.node-type-event div.node .field.field-type-nodereference a 
{ background-color:<?php print designkit_colorshift($color_3, '#ffffff', .9) ?>; 
}
body.node-type-event div.node-content.clear-block.prose div.field.terms.clear-block span.field-label-inline-first 
{ background-color:<?php print designkit_colorshift($color_3, '#eeeeee', .1) ?>; 
}
body.node-type-event div.node-content div.field-items div.field-item.odd div.field-label-inline-first 
{ background-color:<?php print designkit_colorshift($color_3, '#eeeeee', .1) ?>; }

body.node-type-event #right .page-region .block .block-title 
{ background-color:<?php print designkit_colorshift($color_3, '#eeeeee', .1) ?>; }
body.node-type-event #right div.litecal .litecal-title h3.litecal-title { color:<?php print designkit_colorshift($color_3, '#eeeeee', .1) ?>; }




body.page-casetracker .atrium-welcome-links a:hover,
body.page-casetracker .dropdown-blocks .block-toggle li a:hover,
body.page-casetracker .dropdown-blocks .block-toggle li a:focus,
body.page-casetracker .dropdown-blocks .block-toggle ul.links li a:hover,
body.page-casetracker .dropdown-blocks .block-toggle ul.links li a:focus,
body.page-casetracker .dropdown-blocks .block-toggle .item-list li a:hover,
body.page-casetracker .dropdown-blocks .block-toggle .item-list li a:focus,
body.page-casetracker .pager li.pager-current,
body.page-casetracker .more-link a:hover,
body.page-casetracker #global,
body.page-casetracker #navigation { background-color:<?php print $color_5 ?>; }
body.page-casetracker .atrium-welcome-links a:hover,
body.page-casetracker .dropdown-blocks .block-toggle li a:hover,
body.page-casetracker .dropdown-blocks .block-toggle li a:focus,
body.page-casetracker .dropdown-blocks .block-toggle ul.links li a:hover,
body.page-casetracker .dropdown-blocks .block-toggle ul.links li a:focus,
body.page-casetracker .dropdown-blocks .block-toggle .item-list li a:hover,
body.page-casetracker .dropdown-blocks .block-toggle .item-list li a:focus,
body.page-casetracker .pager li.pager-current,
body.page-casetracker .more-link a:hover,
body.page-casetracker #global,
body.page-casetracker #navigation { background-color:<?php print $color_5 ?>; }
body.page-casetracker #header .block-widget .block-content,
body.page-casetracker #header .block .block-title { background-color:<?php print designkit_colorshift($color_5, '#000000', .15) ?>; }
body.page-casetracker .page-region .block .block-title {
  background-color:<?php print designkit_colorshift($color_5, '#eeeeee', .1) ?>;
  border-color:<?php print designkit_colorshift($color_5, '#dddddd', .1) ?>;
  border-bottom-color:<?php print designkit_colorshift($color_5, '#cccccc',.1) ?>;
  }
body.page-casetracker h2.node-title a, 
body.page-casetracker .page-blog .view-blog-listing h2.node-title a 
{ color:<?php print designkit_colorshift($color_5, '#666666', 1.0) ?>; 
}
body.page-casetracker .node-submitted a.username 
{ color:<?php print designkit_colorshift($color_5, '#ffffff', .25) ?>; 
}
body.page-casetracker div.node .field.terms li, 
body.page-casetracker div.node .field.field-type-nodereference a 
{ background-color:<?php print designkit_colorshift($color_5, '#ffffff', .9) ?>; 
}
body.page-casetracker div.node-content.clear-block.prose div.field.terms.clear-block span.field-label-inline-first 
{ background-color:<?php print designkit_colorshift($color_5, '#eeeeee', .1) ?>; 
}
body.page-casetracker div.node-content div.field-items div.field-item.odd div.field-label-inline-first 
{ background-color:<?php print designkit_colorshift($color_5, '#eeeeee', .1) ?>; }



body.node-type-casetracker-basic-case .atrium-welcome-links a:hover,
body.node-type-casetracker-basic-case .dropdown-blocks .block-toggle li a:hover,
body.node-type-casetracker-basic-case .dropdown-blocks .block-toggle li a:focus,
body.node-type-casetracker-basic-case .dropdown-blocks .block-toggle ul.links li a:hover,
body.node-type-casetracker-basic-case .dropdown-blocks .block-toggle ul.links li a:focus,
body.node-type-casetracker-basic-case .dropdown-blocks .block-toggle .item-list li a:hover,
body.node-type-casetracker-basic-case .dropdown-blocks .block-toggle .item-list li a:focus,
body.node-type-casetracker-basic-case .pager li.pager-current,
body.node-type-casetracker-basic-case .more-link a:hover,
body.node-type-casetracker-basic-case #global,
body.node-type-casetracker-basic-case #navigation { background-color:<?php print $color_5 ?>; }
body.node-type-casetracker-basic-case .atrium-welcome-links a:hover,
body.node-type-casetracker-basic-case .dropdown-blocks .block-toggle li a:hover,
body.node-type-casetracker-basic-case .dropdown-blocks .block-toggle li a:focus,
body.node-type-casetracker-basic-case .dropdown-blocks .block-toggle ul.links li a:hover,
body.node-type-casetracker-basic-case .dropdown-blocks .block-toggle ul.links li a:focus,
body.node-type-casetracker-basic-case .dropdown-blocks .block-toggle .item-list li a:hover,
body.node-type-casetracker-basic-case .dropdown-blocks .block-toggle .item-list li a:focus,
body.node-type-casetracker-basic-case .pager li.pager-current,
body.node-type-casetracker-basic-case .more-link a:hover,
body.node-type-casetracker-basic-case #global,
body.node-type-casetracker-basic-case #navigation { background-color:<?php print $color_5 ?>; }
body.node-type-casetracker-basic-case #header .block-widget .block-content,
body.node-type-casetracker-basic-case #header .block .block-title { background-color:<?php print designkit_colorshift($color_5, '#000000', .15) ?>; }
body.node-type-casetracker-basic-case .page-region .block .block-title {
  background-color:<?php print designkit_colorshift($color_5, '#eeeeee', .1) ?>;
  border-color:<?php print designkit_colorshift($color_5, '#dddddd', .1) ?>;
  border-bottom-color:<?php print designkit_colorshift($color_5, '#cccccc',.1) ?>;
  }
body.node-type-casetracker-basic-case h2.node-title a, 
body.node-type-casetracker-basic-case .page-blog .view-blog-listing h2.node-title a 
{ color:<?php print designkit_colorshift($color_5, '#666666', 1.0) ?>; 
}
body.node-type-casetracker-basic-case .node-submitted a.username 
{ color:<?php print designkit_colorshift($color_5, '#ffffff', .25) ?>; 
}
body.node-type-casetracker-basic-case div.node .field.terms li, 
body.node-type-casetracker-basic-case div.node .field.field-type-nodereference a 
{ background-color:<?php print designkit_colorshift($color_5, '#ffffff', .9) ?>; 
}
body.node-type-casetracker-basic-case div.node-content.clear-block.prose div.field.terms.clear-block span.field-label-inline-first 
{ background-color:<?php print designkit_colorshift($color_5, '#eeeeee', .1) ?>; 
}
body.node-type-casetracker-basic-case div.node-content div.field-items div.field-item.odd div.field-label-inline-first 
{ background-color:<?php print designkit_colorshift($color_5, '#eeeeee', .1) ?>; }





body.node-type-casetracker-basic-project .atrium-welcome-links a:hover,
body.node-type-casetracker-basic-project .dropdown-blocks .block-toggle li a:hover,
body.node-type-casetracker-basic-project .dropdown-blocks .block-toggle li a:focus,
body.node-type-casetracker-basic-project .dropdown-blocks .block-toggle ul.links li a:hover,
body.node-type-casetracker-basic-project .dropdown-blocks .block-toggle ul.links li a:focus,
body.node-type-casetracker-basic-project .dropdown-blocks .block-toggle .item-list li a:hover,
body.node-type-casetracker-basic-project .dropdown-blocks .block-toggle .item-list li a:focus,
body.node-type-casetracker-basic-project .pager li.pager-current,
body.node-type-casetracker-basic-project .more-link a:hover,
body.node-type-casetracker-basic-project #global,
body.node-type-casetracker-basic-project #navigation { background-color:<?php print $color_5 ?>; }
body.node-type-casetracker-basic-project .atrium-welcome-links a:hover,
body.node-type-casetracker-basic-project .dropdown-blocks .block-toggle li a:hover,
body.node-type-casetracker-basic-project .dropdown-blocks .block-toggle li a:focus,
body.node-type-casetracker-basic-project .dropdown-blocks .block-toggle ul.links li a:hover,
body.node-type-casetracker-basic-project .dropdown-blocks .block-toggle ul.links li a:focus,
body.node-type-casetracker-basic-project .dropdown-blocks .block-toggle .item-list li a:hover,
body.node-type-casetracker-basic-project .dropdown-blocks .block-toggle .item-list li a:focus,
body.node-type-casetracker-basic-project .pager li.pager-current,
body.node-type-casetracker-basic-project .more-link a:hover,
body.node-type-casetracker-basic-project #global,
body.node-type-casetracker-basic-project #navigation { background-color:<?php print $color_5 ?>; }
body.node-type-casetracker-basic-project #header .block-widget .block-content,
body.node-type-casetracker-basic-project #header .block .block-title { background-color:<?php print designkit_colorshift($color_5, '#000000', .15) ?>; }
body.node-type-casetracker-basic-project .page-region .block .block-title {
  background-color:<?php print designkit_colorshift($color_5, '#eeeeee', .1) ?>;
  border-color:<?php print designkit_colorshift($color_5, '#dddddd', .1) ?>;
  border-bottom-color:<?php print designkit_colorshift($color_5, '#cccccc',.1) ?>;
  }
body.node-type-casetracker-basic-project h2.node-title a, 
body.node-type-casetracker-basic-project .page-blog .view-blog-listing h2.node-title a 
{ color:<?php print designkit_colorshift($color_5, '#666666', 1.0) ?>; 
}
body.node-type-casetracker-basic-project .node-submitted a.username 
{ color:<?php print designkit_colorshift($color_5, '#ffffff', .25) ?>; 
}
body.node-type-casetracker-basic-project div.node .field.terms li, 
body.node-type-casetracker-basic-project div.node .field.field-type-nodereference a 
{ background-color:<?php print designkit_colorshift($color_5, '#ffffff', .9) ?>; 
}
body.node-type-casetracker-basic-project div.node-content.clear-block.prose div.field.terms.clear-block span.field-label-inline-first 
{ background-color:<?php print designkit_colorshift($color_5, '#eeeeee', .1) ?>; 
}
body.node-type-casetracker-basic-project div.node-content div.field-items div.field-item.odd div.field-label-inline-first 
{ background-color:<?php print designkit_colorshift($color_5, '#eeeeee', .1) ?>; }





body.page-user .atrium-welcome-links a:hover,
body.page-user .dropdown-blocks .block-toggle li a:hover,
body.page-user .dropdown-blocks .block-toggle li a:focus,
body.page-user .dropdown-blocks .block-toggle ul.links li a:hover,
body.page-user .dropdown-blocks .block-toggle ul.links li a:focus,
body.page-user .dropdown-blocks .block-toggle .item-list li a:hover,
body.page-user .dropdown-blocks .block-toggle .item-list li a:focus,
body.page-user .pager li.pager-current,
body.page-user .more-link a:hover,
body.page-user #global,
body.page-user  #navigation { background-color:<?php print $color_6 ?>; }
body.page-user  #header .block-widget .block-content,
body.page-user  #header .block .block-title { background-color:<?php print designkit_colorshift($color_6, '#000000', .15) ?>; }
body.page-user  .page-region .block .block-title {
  background-color:<?php print designkit_colorshift($color_6, '#eeeeee', .1) ?>;
  border-color:<?php print designkit_colorshift($color_6, '#dddddd', .1) ?>;
  border-bottom-color:<?php print designkit_colorshift($color_6, '#cccccc',.1) ?>;
  }
body.page-user  h2.node-title a, 
body.page-user .page-blog .view-blog-listing h2.node-title a 
{ color:<?php print designkit_colorshift($color_6, '#666666', 1.0) ?>; 
}
body.page-user .node-submitted a.username 
{ color:<?php print designkit_colorshift($color_6, '#ffffff', .25) ?>; 
}
body.page-user div.node .field.terms li, 
body.page-user div.node .field.field-type-nodereference a 
{ background-color:<?php print designkit_colorshift($color_6, '#ffffff', .9) ?>; 
}
body.page-user div.node-content.clear-block.prose div.field.terms.clear-block span.field-label-inline-first 
{ background-color:<?php print designkit_colorshift($color_6, '#eeeeee', .1) ?>; 
}
body.page-user div.node-content div.field-items div.field-item.odd div.field-label-inline-first 
{ background-color:<?php print designkit_colorshift($color_6, '#eeeeee', .1) ?>; }

body.page-user  span.spaces-feature { background-color:<?php print designkit_colorshift($color_6, '#eeeeee', .15) ?>; }

body.page-user   div.litecal .litecal-title { color:<?php print designkit_colorshift($color_6, '#eeeeee', .1) ?>; 
    color:;
}


body.page-members .atrium-welcome-links a:hover,
body.page-members .dropdown-blocks .block-toggle li a:hover,
body.page-members .dropdown-blocks .block-toggle li a:focus,
body.page-members .dropdown-blocks .block-toggle ul.links li a:hover,
body.page-members .dropdown-blocks .block-toggle ul.links li a:focus,
body.page-members .dropdown-blocks .block-toggle .item-list li a:hover,
body.page-members .dropdown-blocks .block-toggle .item-list li a:focus,
body.page-members .pager li.pager-current,
body.page-members .more-link a:hover,
body.page-members #global,
body.page-members  #navigation { background-color:<?php print $color_6 ?>; }
body.page-members  #header .block-widget .block-content,
body.page-members  #header .block .block-title { background-color:<?php print designkit_colorshift($color_6, '#000000', .15) ?>; }
body.page-members  .page-region .block .block-title {
  background-color:<?php print designkit_colorshift($color_6, '#eeeeee', .1) ?>;
  border-color:<?php print designkit_colorshift($color_6, '#dddddd', .1) ?>;
  border-bottom-color:<?php print designkit_colorshift($color_6, '#cccccc',.1) ?>;
  }
body.page-members  h2.node-title a, 
body.page-members .page-blog .view-blog-listing h2.node-title a 
{ color:<?php print designkit_colorshift($color_6, '#666666', 1.0) ?>; 
}
body.page-members .node-submitted a.username 
{ color:<?php print designkit_colorshift($color_6, '#ffffff', .25) ?>; 
}
body.page-members div.node .field.terms li, 
body.page-members div.node .field.field-type-nodereference a 
{ background-color:<?php print designkit_colorshift($color_6, '#ffffff', .9) ?>; 
}
body.page-members div.node-content.clear-block.prose div.field.terms.clear-block span.field-label-inline-first 
{ background-color:<?php print designkit_colorshift($color_6, '#eeeeee', .1) ?>; 
}
body.page-members div.node-content div.field-items div.field-item.odd div.field-label-inline-first 
{ background-color:<?php print designkit_colorshift($color_6, '#eeeeee', .1) ?>; }

body.page-members  span.spaces-feature { background-color:<?php print designkit_colorshift($color_6, '#eeeeee', .15) ?>; }

body.page-members   div.litecal .litecal-title { color:<?php print designkit_colorshift($color_6, '#eeeeee', .1) ?>; 
    color:;
}


body.page-og .atrium-welcome-links a:hover,
body.page-og .dropdown-blocks .block-toggle li a:hover,
body.page-og .dropdown-blocks .block-toggle li a:focus,
body.page-og .dropdown-blocks .block-toggle ul.links li a:hover,
body.page-og .dropdown-blocks .block-toggle ul.links li a:focus,
body.page-og .dropdown-blocks .block-toggle .item-list li a:hover,
body.page-og .dropdown-blocks .block-toggle .item-list li a:focus,
body.page-og .pager li.pager-current,
body.page-og .more-link a:hover,
body.page-og #global,
body.page-og  #navigation { background-color:<?php print $color_7 ?>; }
body.page-og  #header .block-widget .block-content,
body.page-og  #header .block .block-title { background-color:<?php print designkit_colorshift($color_7, '#000000', .15) ?>; }
body.page-og  .page-region .block .block-title {
  background-color:<?php print designkit_colorshift($color_7, '#eeeeee', .1) ?>;
  border-color:<?php print designkit_colorshift($color_7, '#dddddd', .1) ?>;
  border-bottom-color:<?php print designkit_colorshift($color_7, '#cccccc',.1) ?>;
  }
















.drilldown .trail a {
  border-top-color:<?php print designkit_colorshift($background, '#ffffff', .95) ?>;
  border-bottom-color:<?php print designkit_colorshift($background, '#dddddd', .95) ?>;
  background-color:<?php print designkit_colorshift($background, '#eeeeee', .95) ?>;
  }

.decay-10 .comment-title { border-color:<?php print designkit_colorshift($background, '#eeeeee', 1) ?>; }
.decay-9 .comment-title { border-color:<?php print designkit_colorshift($background, '#eeeeee', .9) ?>; }
.decay-8 .comment-title { border-color:<?php print designkit_colorshift($background, '#eeeeee', .8) ?>; }
.decay-7 .comment-title { border-color:<?php print designkit_colorshift($background, '#eeeeee', .7) ?>; }
.decay-6 .comment-title { border-color:<?php print designkit_colorshift($background, '#eeeeee', .6) ?>; }
.decay-5 .comment-title { border-color:<?php print designkit_colorshift($background, '#eeeeee', .5) ?>; }
.decay-4 .comment-title { border-color:<?php print designkit_colorshift($background, '#eeeeee', .4) ?>; }
.decay-3 .comment-title { border-color:<?php print designkit_colorshift($background, '#eeeeee', .3) ?>; }
.decay-2 .comment-title { border-color:<?php print designkit_colorshift($background, '#eeeeee', .2) ?>; }
.decay-1 .comment-title { border-color:<?php print designkit_colorshift($background, '#eeeeee', .1) ?>; }
.decay-0 .comment-title { border-color:<?php print $background;?>; }
</style>




