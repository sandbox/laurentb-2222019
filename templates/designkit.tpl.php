
<style type='text/css'>

.atrium-welcome-links a:hover,
.dropdown-blocks .block-toggle li a:hover,
.dropdown-blocks .block-toggle li a:focus,
.dropdown-blocks .block-toggle ul.links li a:hover,
 .dropdown-blocks .block-toggle ul.links li a:focus,
 .dropdown-blocks .block-toggle .item-list li a:hover,
 .dropdown-blocks .block-toggle .item-list li a:focus,
 .pager li.pager-current,
.more-link a:hover,
 #global,
  #navigation { background-color:<?php print $background ?>; }

  #header .block-widget .block-content,
  #header .block .block-title { background-color:<?php print designkit_colorshift($background, '#000000', .15) ?>; }
  .page-region .block .block-title {
  background-color:<?php print designkit_colorshift($background, '#eeeeee', .1) ?>;
  border-color:<?php print designkit_colorshift($background, '#dddddd', .1) ?>;
  border-bottom-color:<?php print designkit_colorshift($background, '#cccccc',.1) ?>;
  }
h2.node-title a, 
.page-blog .view-blog-listing h2.node-title a 
{ color:<?php print designkit_colorshift($background, '#666666', 1.0) ?>; 
}
.node-submitted a.username 
{ color:<?php print designkit_colorshift($background, '#ffffff', .25) ?>; 
}
div.node .field.terms li, 
div.node .field.field-type-nodereference a 
{ background-color:<?php print designkit_colorshift($background, '#ffffff', .9) ?>; 
}
div.node-content.clear-block.prose div.field.terms.clear-block span.field-label-inline-first 
{ background-color:<?php print designkit_colorshift($background, '#eeeeee', .1) ?>; 
}
div.node-content div.field-items div.field-item.odd div.field-label-inline-first 
{ background-color:<?php print designkit_colorshift($background, '#eeeeee', .1) ?>; }

span.spaces-feature { background-color:<?php print designkit_colorshift($background, '#eeeeee', .15) ?>; }



body.page-dashboard .atrium-welcome-links a:hover,
body.page-dashboard .dropdown-blocks .block-toggle li a:hover,
body.page-dashboard .dropdown-blocks .block-toggle li a:focus,
body.page-dashboard .dropdown-blocks .block-toggle ul.links li a:hover,
body.page-dashboard .dropdown-blocks .block-toggle ul.links li a:focus,
body.page-dashboard .dropdown-blocks .block-toggle .item-list li a:hover,
body.page-dashboard .dropdown-blocks .block-toggle .item-list li a:focus,
body.page-dashboard .pager li.pager-current,
body.page-dashboard .more-link a:hover,
body.page-dashboard #global,
body.page-dashboard  #navigation { background-color:<?php print $background ?>; }
body.page-dashboard  #header .block-widget .block-content,
body.page-dashboard  #header .block .block-title { background-color:<?php print designkit_colorshift($background, '#000000', .15) ?>; }
body.page-dashboard  .page-region .block .block-title {
  background-color:<?php print designkit_colorshift($background, '#eeeeee', .1) ?>;
  border-color:<?php print designkit_colorshift($background, '#dddddd', .1) ?>;
  border-bottom-color:<?php print designkit_colorshift($background, '#cccccc',.1) ?>;
  }
body.page-dashboard  h2.node-title a, 
body.page-dashboard .page-blog .view-blog-listing h2.node-title a 
{ color:<?php print designkit_colorshift($background, '#666666', 1.0) ?>; 
}
body.page-dashboard .node-submitted a.username 
{ color:<?php print designkit_colorshift($background, '#ffffff', .25) ?>; 
}
body.page-dashboard div.node .field.terms li, 
body.page-dashboard div.node .field.field-type-nodereference a 
{ background-color:<?php print designkit_colorshift($background, '#ffffff', .9) ?>; 
}
body.page-dashboard div.node-content.clear-block.prose div.field.terms.clear-block span.field-label-inline-first 
{ background-color:<?php print designkit_colorshift($background, '#eeeeee', .1) ?>; 
}
body.page-dashboard div.node-content div.field-items div.field-item.odd div.field-label-inline-first 
{ background-color:<?php print designkit_colorshift($background, '#eeeeee', .1) ?>; }

body.page-dashboard  span.spaces-feature { background-color:<?php print designkit_colorshift($background, '#eeeeee', .15) ?>; }

body.page-dashboard   div.litecal .litecal-title { color:<?php print designkit_colorshift($background, '#eeeeee', .1) ?>; 
    color:;
}

body.page-dashboard #block-views-blog_comments-block_1 div.views-field.related-title a:before { background-color:<?php print designkit_colorshift($background, '#eeeeee', .1) ?>; }
table.cases tr.mine td.username { background-color:<?php print designkit_colorshift($background, '#eeeeee', .5) ?>; }



body .atrium-welcome-links a:hover,
body .dropdown-blocks .block-toggle li a:hover,
body .dropdown-blocks .block-toggle li a:focus,
body .dropdown-blocks .block-toggle ul.links li a:hover,
body .dropdown-blocks .block-toggle ul.links li a:focus,
body .dropdown-blocks .block-toggle .item-list li a:hover,
body .dropdown-blocks .block-toggle .item-list li a:focus,
body .pager li.pager-current,
body .more-link a:hover,
body #global,
body #navigation { background-color:<?php print $background ?>; }
body #header .block-widget .block-content,
body #header .block .block-title { background-color:<?php print designkit_colorshift($background, '#000000', .15) ?>; }
body .page-region .block .block-title {
  background-color:<?php print designkit_colorshift($background, '#eeeeee', .1) ?>;
  border-color:<?php print designkit_colorshift($background, '#dddddd', .1) ?>;
  border-bottom-color:<?php print designkit_colorshift($background, '#cccccc',.1) ?>;
  }
body h2.node-title a, 
body .page-blog .view-blog-listing h2.node-title a 
{ color:<?php print designkit_colorshift($background, '#666666', 1.0) ?>; 
}
body .node-submitted a.username 
{ color:<?php print designkit_colorshift($background, '#ffffff', .25) ?>; 
}
body div.node .field.terms li, 
body div.node .field.field-type-nodereference a 
{ background-color:<?php print designkit_colorshift($background, '#ffffff', .9) ?>; 
}
body div.node-content.clear-block.prose div.field.terms.clear-block span.field-label-inline-first 
{ background-color:<?php print designkit_colorshift($background, '#eeeeee', .1) ?>; 
}
body div.node-content div.field-items div.field-item.odd div.field-label-inline-first 
{ background-color:<?php print designkit_colorshift($background, '#eeeeee', .1) ?>; }
body h2.comment-title a { background-color:<?php print designkit_colorshift($background, '#eeeeee', .1) ?>; 
}


body .atrium-welcome-links a:hover,
body .dropdown-blocks .block-toggle li a:hover,
body .dropdown-blocks .block-toggle li a:focus,
body .dropdown-blocks .block-toggle ul.links li a:hover,
body .dropdown-blocks .block-toggle ul.links li a:focus,
body .dropdown-blocks .block-toggle .item-list li a:hover,
body .dropdown-blocks .block-toggle .item-list li a:focus,
body .pager li.pager-current,
body .more-link a:hover,
body #global,
body #navigation { background-color:<?php print $background ?>; }
body #header .block-widget .block-content,
body #header .block .block-title { background-color:<?php print designkit_colorshift($background, '#000000', .15) ?>; }
body .page-region .block .block-title {
  background-color:<?php print designkit_colorshift($background, '#eeeeee', .1) ?>;
  border-color:<?php print designkit_colorshift($background, '#dddddd', .1) ?>;
  border-bottom-color:<?php print designkit_colorshift($background, '#cccccc',.1) ?>;
  }
body h2.node-title a, 
body .page-blog .view-blog-listing h2.node-title a 
{ color:<?php print designkit_colorshift($background, '#666666', 1.0) ?>; 
}
body .node-submitted a.username 
{ color:<?php print designkit_colorshift($background, '#ffffff', .25) ?>; 
}
body div.node .field.terms li, 
body div.node .field.field-type-nodereference a 
{ background-color:<?php print designkit_colorshift($background, '#ffffff', .9) ?>; 
}
body div.node-content.clear-block.prose div.field.terms.clear-block span.field-label-inline-first 
{ background-color:<?php print designkit_colorshift($background, '#eeeeee', .1) ?>; 
}
body div.node-content div.field-items div.field-item.odd div.field-label-inline-first 
{ background-color:<?php print designkit_colorshift($background, '#eeeeee', .1) ?>; }



.drilldown .trail a {
  border-top-color:<?php print designkit_colorshift($background, '#ffffff', .95) ?>;
  border-bottom-color:<?php print designkit_colorshift($background, '#dddddd', .95) ?>;
  background-color:<?php print designkit_colorshift($background, '#eeeeee', .95) ?>;
  }

.decay-10 .comment-title { border-color:<?php print designkit_colorshift($background, '#eeeeee', 1) ?>; }
.decay-9 .comment-title { border-color:<?php print designkit_colorshift($background, '#eeeeee', .9) ?>; }
.decay-8 .comment-title { border-color:<?php print designkit_colorshift($background, '#eeeeee', .8) ?>; }
.decay-7 .comment-title { border-color:<?php print designkit_colorshift($background, '#eeeeee', .7) ?>; }
.decay-6 .comment-title { border-color:<?php print designkit_colorshift($background, '#eeeeee', .6) ?>; }
.decay-5 .comment-title { border-color:<?php print designkit_colorshift($background, '#eeeeee', .5) ?>; }
.decay-4 .comment-title { border-color:<?php print designkit_colorshift($background, '#eeeeee', .4) ?>; }
.decay-3 .comment-title { border-color:<?php print designkit_colorshift($background, '#eeeeee', .3) ?>; }
.decay-2 .comment-title { border-color:<?php print designkit_colorshift($background, '#eeeeee', .2) ?>; }
.decay-1 .comment-title { border-color:<?php print designkit_colorshift($background, '#eeeeee', .1) ?>; }
.decay-0 .comment-title { border-color:<?php print $background;?>; }
</style>




