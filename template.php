<?php

/**
 * Implementation of hook_theme().
 */

function phptemplate_form_element($element, $value) {
  $output  = '<div class="form-item">'."\n";
  $required = !empty($element['#required']) ? '<span class="form-required" title="'. t('This field is required.') .'">*</span>' : '';
  if (!empty($element['#title'])) {
    $title = $element['#title'];
    if (!empty($element['#id'])) {
      $output .= ' <label for="'. $element['#id'] .'">'. t('!title !required', array('!title' => filter_xss_admin($title), '!required' => $required)) ."</label>\n";
    }
    else {
      $output .= ' <label>'. t('!title !required', array('!title' => filter_xss_admin($title), '!required' => $required)) ."</label>\n";
    }
  }
  $output .= " $value\n";
  if (!empty($element['#description'])) {
    $output .= ' <div class="description">'. $element['#description'] ."</div>\n";
  }
  $output .= "</div>\n";
  return $output;
}


/**
 * Format a checkbox.
 *
 * @param $element
 *   An associative array containing the properties of the element.
 *   Properties used:  title, value, return_value, description, required
 * @return
 *   A themed HTML string representing the checkbox.
 *
 * @ingroup themeable
 */

function planeo_checkbox($element) {
  _form_set_class($element, array('form-checkbox'));
  $checkbox = '<input ';
  $checkbox .= 'type="checkbox" ';
  $checkbox .= 'name="' . $element['#name'] . '" ';
  $checkbox .= 'id="' . $element['#id'] . '" ';
  $checkbox .= 'value="' . $element['#return_value'] . '" ';
  $checkbox .= $element['#value'] ? ' checked="checked" ' : ' ';
  $checkbox .= drupal_attributes($element['#attributes']) . ' />';
  if (!

is_null($element['#title'])) {
    $checkbox .= '<label class="option" for="' . $element['#id'] . '">' . $element['#title'] . '</label>';
  }
  unset(

$element['#title']);
  return theme('form_element', $element, $checkbox);
}




/**
 * Format a radio button.
 *
 * @param $element
 *   An associative array containing the properties of the element.
 *   Properties used: required, return_value, value, attributes, title, description
 * @return
 *   A themed HTML string representing the form item group.
 *
 * @ingroup themeable
 */


function planeo_radio($element) {
  _form_set_class($element, array('form-radio'));
  $output = '<input type="radio" ';
  $output .= 'id="'. $element['#id'] .'" ';
  $output .= 'name="'. $element['#name'] .'" ';
  $output .= 'value="'. $element['#return_value'] .'" ';
  $output .= (check_plain($element['#value']) == $element['#return_value']) ? ' checked="checked" ' : ' ';
  $output .= drupal_attributes($element['#attributes']) .' />';
  if (!is_null($element['#title'])) {
    $output .= '<label class="option" for="'. $element['#id'] .'"> '. $element['#title'] .'</label>';
  }

  unset($element['#title']);
  return theme('form_element', $element, $output);
}


/**
 * Format a dropdown menu or scrolling selection box.
 *
 * @param $element
 *   An associative array containing the properties of the element.
 *   Properties used: title, value, options, description, extra, multiple, required
 * @return
 *   A themed HTML string representing the form element.
 *
 * @ingroup themeable
 *
 * It is possible to group options together; to do this, change the format of
 * $options to an associative array in which the keys are group labels, and the
 * values are associative arrays in the normal $options format.
 */
function planeo_select($element) {
  $select = '';
  $size = $element['#size'] ? ' size="'. $element['#size'] .'"' : '';
  _form_set_class($element, array('form-select'));
  $multiple = $element['#multiple'];


  //return theme('form_element', $element, '<select name="'. $element['#name'].''. ($multiple ? '[]' : '') .'"'. ($multiple ? ' multiple="multiple" ' : '') . drupal_attributes($element['#attributes']) .' id="'. $element['#id'] .'" '. $size .'>'. planeo_form_select_options($element) .'</select>');

  return theme('form_element', $element, '<select name="'. $element['#name'].''. ($multiple ? '[]' : '') .'"' . drupal_attributes($element['#attributes']) .' id="'. $element['#id'] .'" >'. planeo_form_select_options($element) .'</select>');
}

function planeo_form_select_options($element, $choices = NULL) {
  if (!isset($choices)) {
    $choices = $element['#options'];
  }
  // array_key_exists() accommodates the rare event where $element['#value'] is NULL.
  // isset() fails in this situation.
  $value_valid = isset($element['#value']) || array_key_exists('#value', $element);
  $value_is_array = is_array($element['#value']);
  $options = '';
  foreach ($choices as $key => $choice) {
    if (is_array($choice)) {
      $options .= '<optgroup label="'. $key .'">';
      $options .= form_select_options($element, $choice);
      $options .= '</optgroup>';
    }
    elseif (is_object($choice)) {
      $options .= form_select_options($element, $choice->option);
    }
    else {
      $key = (string)$key;
      if ($value_valid && (!$value_is_array && (string)$element['#value'] === $key || ($value_is_array && in_array($key, $element['#value'])))) {
        $selected = ' selected="selected"';
      }
      else {
        $selected = '';
      }

      $choice = str_replace('<','',$choice);
      $choice = str_replace('>','',$choice);

      $options .= '<option value="'. check_plain($key) .'"'. $selected .'>'. check_plain($choice) .'</option>';

    }
  }
  return $options;
}


function planeo_preprocess_html(&$vars) {
  $vars['classes_array'][] = 'new-class';
}


/**
 * Preprocessor for theme_page().
 */
function planeo_preprocess_page(&$vars) {
  

  // Add body class for layout.
  if (strpos($_SERVER['REQUEST_URI'], 'dashboard')) {
    // do something

  $vars['attr']['class'] .= '_dashboard';
  }

  // Add body class for layout.
  if (strpos($_SERVER['REQUEST_URI'], 'add')) {
    // do something

  $vars['attr']['class'] .= '_add';
  }
  
  // Add body class for layout.
  if (strpos($_SERVER['REQUEST_URI'], 'features')) {
    // do something

  $vars['attr']['class'] .= '_features';
  }

  // Add body class for layout.
  if (strpos($_SERVER['REQUEST_URI'], 'notifications')) {
    // do something

  $vars['attr']['class'] .= '_notifications';
  }

  // Add body class for layout.
  if (strpos($_SERVER['REQUEST_URI'], 'openid')) {
    // do something

  $vars['attr']['class'] .= '_openid';
  }

  // Add body class for layout.
  if (strpos($_SERVER['REQUEST_URI'], 'revisions')) {
    // do something

  $vars['attr']['class'] .= '_revisions';
  }
  
  

  
}

/**
 * Preprocessor for theme_node().
 */
function planeo_preprocess_node(&$vars) {
  if (!empty($vars['terms'])) {

    $vars['content'] = str_replace('Tagged:</span>','Tagged</span>',$vars['content']);

  }





  $vars['title'] = check_plain($vars['node']->title);
  $vars['layout'] = FALSE;

  // Add node-page class.
  $vars['attr']['class'] .= $vars['node'] === menu_get_object() ? ' node-page' : '';

  // Don't show the full node when a comment is being previewed.
  $vars = context_get('comment', 'preview') == TRUE ? array() : $vars;

  // Clear out catchall template file suggestions like those made by og.
  // TODO refactor
  if (!empty($vars['template_files'])) {
    foreach ($vars['template_files'] as $k => $f) {
      if (strpos($f, 'node-'.$vars['type']) === FALSE) {
        unset($vars['template_files'][$k]);
      }
    }
  }
}







